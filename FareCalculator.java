import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;



public class FareCalculator extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		// 1. Create & configure user interface controls
		
		//Heading
		Label Heading=new Label("Airport Ride calculator");
		
		//From
	    Label From =new Label("From?");
	    
	    //Textfield to take input for either brampton or cester
	    TextField FromTextField=  new TextField();	   
	    
	    //Checkboxes for selection 
	    CheckBox ExtraLagguage=new CheckBox("Extra Lagguage?");
	    CheckBox Pet=new CheckBox("Pets?");
	    CheckBox ETR=new CheckBox("Use 407 ETR?");  
	    CheckBox   Tip=new CheckBox("Add Tip?");
	    
	    //Button to calculate
	    Button   btn=new Button("Calculate");  
	   
	        
	       

		// results
		Label resultsLabel = new Label("");
		
		// Add event handler for button
		btn.setOnAction(new EventHandler<ActionEvent>() {
		    @Override
		    public void handle(ActionEvent e) {
		    	 double Amount=0, Tip1=0,Total =0,Tax=0;
		    	 String city=FromTextField.getText();
		    	 
		    	 //Checking if String is empty
		    	 if(city.isEmpty()) 
		    	 {
		    		 resultsLabel.setText("Please Enter Either brampton or cester");
		    		 
		    	 }
		    	 
		    	 else  
		    	 //code to calculate fare
		    	 {
		    		 if(city.equalsIgnoreCase("cester"))
			    	 {
			    		 Amount+=51;
			    		 
			    		 if(ETR.isSelected())
			    		 {
			    			 Amount=Amount+(0.25*21);
			    		 }
			    	 }
		    		 
		    	 if(city.equalsIgnoreCase("brampton"))
		    	 {
		    		 
		    		 Amount+=38; 
		    		 if(ETR.isSelected())
		    		 {
		    			 Amount=Amount+(0.25*9.9);
		    		 }
		    	 }
		    	 
		         if(ExtraLagguage.isSelected())
		         {  
		             Amount+=10;    
		         } 
		         if(Pet.isSelected())
		         {  
		             Amount+=6;   
		         }  
		     
		         Tax=Amount *0.13;
		         Total=Amount + Tax;
		         
		         if(Tip.isSelected())
		         {  
		             Tip1+=Total*0.15;    
		         }     
		           Total=Total+Tip1;		
		    		// --------------------------------------
		    		// 3. Output the result to the screen
		    		// --------------------------------------
		 String TotalAmount=String.valueOf(Total);
		    		resultsLabel.setText("The Total Amount is : $" + TotalAmount);
		    	 }	
		    }
		    
		});
		
		
		
		// 2. Make a layout manager
		VBox root = new VBox(10);
		
		// 3. Add controls to the layout manager
		root.getChildren().add(Heading);
		root.getChildren().add(From);
		root.getChildren().add(FromTextField);
		root.getChildren().add(ExtraLagguage);
		root.getChildren().add(Pet);
		root.getChildren().add(ETR);
		root.getChildren().add(Tip);
		root.getChildren().add(btn);
		root.getChildren().add(resultsLabel);
		
		// 5. Add scene to the stage
		primaryStage.setScene(new Scene(root, 250, 300));
		
		// 6. Show the app
		primaryStage.show();
	}
}

